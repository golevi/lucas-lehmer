package llt

import (
	"errors"
	"math/big"
)

var (
	zero *big.Int = big.NewInt(0)
	one  *big.Int = big.NewInt(1)
	two  *big.Int = big.NewInt(2)
)

var (
	ErrNotPrime = errors.New("p is not prime")
)

const randomMillerRabinBases = 1

// Test runs the Lucas-Lehmer test against p.
//
// Test returns
// - a bool indicating whether p is a Mersenne prime.
// - a big.Int with the residue
// - an error if p is not prime
func Test(p uint) (bool, *big.Int, error) {
	if !big.NewInt(int64(p)).ProbablyPrime(randomMillerRabinBases) {
		return false, zero, ErrNotPrime
	}

	var dummy1, dummy2 big.Int

	s := big.NewInt(4)
	m := big.NewInt(0)
	m = m.Sub(m.Lsh(one, p), one)

	for i := 0; i < int(p)-2; i++ {
		s = s.Sub(s.Mul(s, s), two)

		for s.Cmp(m) == 1 {
			s.Add(dummy1.And(s, m), dummy2.Rsh(s, p))
		}
		if s.Cmp(m) == 0 {
			s = zero
		}
	}

	return s.Cmp(zero) == 0, s, nil
}
