package llt

import (
	"math/big"
	"testing"
)

func TestPrimes(t *testing.T) {
	tcs := map[uint]bool{
		3:  true,
		5:  true,
		7:  true,
		11: false,
		13: true,
		17: true,
		19: true,
		23: false,
		29: false,
		31: true,
		37: false,
		41: false,
		43: false,
		47: false,
		53: false,
		59: false,
		61: true,
		67: false,
		71: false,
		89: true,
		97: false,
	}

	for p, tcb := range tcs {
		b, _, _ := Test(p)
		if b != tcb {
			t.Errorf("expected %v, got %v, %d", tcb, b, p)
		}
	}
}

func TestEvens(t *testing.T) {
	tcs := map[uint]bool{
		10:  false,
		12:  false,
		100: false,
	}

	for p, tcb := range tcs {
		b, _, err := Test(p)
		if b != tcb {
			t.Errorf("expected %v, got %v, %d", tcb, b, p)
		}

		if err == nil {
			t.Errorf("expected %v, got nil", ErrNotPrime)
		}
	}
}

func TestResidue(t *testing.T) {
	tcs := map[uint]*big.Int{
		29: big.NewInt(458738443),
	}

	for p, tcb := range tcs {
		_, r, _ := Test(p)
		if r.Cmp(tcb) != 0 {
			t.Errorf("expected %v, got %v", tcb, r)
		}
	}
}
